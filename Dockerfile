# syntax=docker/dockerfile:1

FROM node:fermium-alpine

MAINTAINER Hariharan Narayanan 

# Update and install dependencies
RUN apk update && \
		apk upgrade --available && \
		apk add git

WORKDIR /usr/app

# Clone turtl server
RUN git clone https://github.com/turtl/server.git /usr/app

# Install turtl server
RUN	npm install

# Copy configuration
COPY config/config.yaml config/config.yaml


RUN mkdir -p plugins    # (usually just plugins/ in turtl/server/)
RUN ./scripts/init-db.sh
RUN node server.js
